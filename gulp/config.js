
module.exports.version = {
	file: 'CHANGELOG.md',
	pkg: './package.json',
	dest: './dist/'
}

module.exports.copy = {
	fonts: {
		src: './src/assets/toolkit/fonts/**/*.{ttf,eot,woff}',
		dest: './dist/assets/toolkit/fonts'
	}
}

/**
 * [styles description]
 * @type {Object}
 */
module.exports.styles = {
	fabricator: 'src/assets/fabricator/styles/fabricator.scss',
	toolkit: 'src/assets/toolkit/styles/toolkit.css'
}

/**
 * [images description]
 * @type {String}
 */
module.exports.images = 'src/assets/toolkit/images/**/*';

/**
 * [scripts description]
 * @type {Object}
 */
module.exports.scripts = {
		fabricator: './src/assets/fabricator/scripts/fabricator.js'
}


module.exports.browserify = {
	src: './src/assets/toolkit/scripts/toolkit.js',
	dest: './dist/assets/toolkit/scripts/'
};


module.exports.watch = [
	{ src: 'src/assets/toolkit/styles/**/*.css', task: ['styles:toolkit'] },
	{ src: 'src/**/*.{html,md,json,yml}', task: ['assemble'] },
	{ src: 'src/assets/toolkit/scripts/**/*.js', task: ['browserify']}
];

/**
 * [dest description]
 * @type {String}
 */
module.exports.dest = './dist';