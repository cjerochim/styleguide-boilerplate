'use strict';

var sourcemaps 			= require('gulp-sourcemaps');
var sass 				= require('gulp-sass');
var csso 				= require('gulp-csso');
var browserSync 		= require('browser-sync');
var reload				= browserSync.reload;
var prefix 				= require('gulp-autoprefixer');
var gulpif 				= require('gulp-if');
var rename 				= require('gulp-rename');
var gulp 				= require('gulp');
var config 				= require('../config');
//TODO Add notifcations to build

// styles
gulp.task('styles:fabricator', function () {
	gulp.src(config.styles.fabricator)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		// .pipe(prefix('last 1 version'))
		// .pipe(gulpif(!config.dev, csso()))
		.pipe(rename('f.css'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(config.dest + '/assets/fabricator/styles'))
		// .pipe(gulpif(config.dev, reload({stream:true})));
});

// Note - migrated over to PostCSS.

gulp.task('styles', ['styles:fabricator', 'styles:toolkit']);