'use strict';

var gulp 					= require('gulp');
var webpack 				= require('webpack');
var watch 					= require('gulp-watch');
var batch 					= require('gulp-batch');
var gutil 					= require('gulp-util');
var config 					= require('../config');
var webpackConfig 			= require('../../webpack.config')(config);
var webpackCompiler			= webpack(webpackConfig);

// scripts
gulp.task('scripts', function (done) {
	webpackCompiler.run(function (error, result) {
		if (error) {
			gutil.log(gutil.colors.red(error));
		}
		result = result.toJson();
		if (result.errors.length) {
			result.errors.forEach(function (error) {
				gutil.log(gutil.colors.red(error));
			});
		}
		done();
	});
});


/**
 * Because webpackCompiler.watch() isn't being used
 * manually remove the changed file path from the cache
 */
	
// function webpackCache(e) {
// 	var keys = Object.keys(webpackConfig.cache);
// 	var key, matchedKey;
// 	for (var keyIndex = 0; keyIndex < keys.length; keyIndex++) {
// 		key = keys[keyIndex];
// 		if (key.indexOf(e.path) !== -1) {
// 			matchedKey = key;
// 			break;
// 		}
// 	}
// 	if (matchedKey) {
// 		delete webpackConfig.cache[matchedKey];
// 	}
// }

// gulp.task('scripts:watch', function() {
// 	watch(config.scripts.watch, batch(function(events, done) {
// 		events.on('data', webpackCache).on('end', function() {
// 			gulp.start('scripts', done);
// 		});
// 	}));
// })