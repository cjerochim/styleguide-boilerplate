'use strict';

var gulp 				= require('gulp');
var config 				= require('../config');

gulp.task('copy:fonts', function() {
	return gulp.src(config.copy.fonts.src)
		.pipe(gulp.dest(config.copy.fonts.dest));
});

gulp.task('copy', ['copy:fonts']);