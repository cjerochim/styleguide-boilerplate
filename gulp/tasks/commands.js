'use strict';

var gulp 						= require('gulp');
var runSequence 				= require('run-sequence');
var config		 				= require('../config');
var webpackConfig 				= require('../../webpack.config')(config);


// TODO - SVG' font-icons
// TODO - Accessability checks
// TODO - Copy vendors or image assets if required.
// TOOD - GULP NOTIFY
// TODO - Default content to include on the basic styelguide. reference PatternLab

/**
 * Watch assets and scripts,
 * Note - scripts is separate due to the use of webpack, may consider migrating to browserify
 * @type {[type]}
 */
gulp.task('watch', ['watch:assets']);

/**
 * Standard build tasks without the watch
 */
gulp.task('build', ['styles', 'scripts', 'browserify', 'images', 'assemble']);

/**
 * Versioning release
 * for commit conventions review
 * https://github.com/ajoslin/conventional-changelog/blob/master/conventions/angular.md
 */
// Major increment
gulp.task('version:major', ['version:bump:major', 'version:changelog']);
// Minor increment
gulp.task('version:minor', ['version:bump:minor', 'version:changelog']);

/**
 * Build all assets, watch for any updates and serve assets over browser-sync
 */
gulp.task('default', ['build', 'watch'], function() {
	gulp.start('serve');
})

