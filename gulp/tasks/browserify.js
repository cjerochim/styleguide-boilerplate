'use strict';

var gulp 						= require('gulp');
var browserify 			= require('browserify');
var babelify 				= require('babelify');
var source 					= require('vinyl-source-stream');
var plumber 				= require('gulp-plumber');
var notify 					= require('gulp-notify');
var browserSync 		= require('browser-sync');
var reload					= browserSync.reload;
var config 					= require('../config');
var utils 					= require('../utils');

gulp.task('browserify', function() {
	return browserify({ entries: config.browserify.src, debug: true })
	.transform(babelify, { presets: ['es2015'] })
	.bundle()
		.on('error', utils.errorHandler)
		.pipe(source('toolkit.js'))
		.pipe(gulp.dest(config.browserify.dest))
		.pipe(reload({ stream: true }))
		.pipe(notify({
			title: 'Browserify Complete' ,
			message: 'Yay'
		}))
});