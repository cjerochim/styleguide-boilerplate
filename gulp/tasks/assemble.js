'use strict';


var gulp 					= require('gulp');
var assemble 				= require('fabricator-assemble');
var config 					= require('../config');

// assemble
gulp.task('assemble', function (done) {
	assemble({
		logErrors: config.dev
	});
	done();
});