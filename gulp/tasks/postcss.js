'use strict';

var gulp 						= require('gulp');
var postcss 				= require('gulp-postcss');
var pxtoem 					= require('postcss-px-to-em');
var plumber 				= require('gulp-plumber');
var autoprefixer		= require('autoprefixer');
var rucksack 				= require('rucksack-css');
var customMedia 		= require('postcss-custom-media');
var precss 					= require('precss');
var customNested 		= require('postcss-nested');
var browserSync 		= require('browser-sync');
var reload					= browserSync.reload;
var lost 						= require('lost');
var config 					= require('../config');
var utils 					= require('../utils');

gulp.task('styles:toolkit', function() {
	var processors = [
		autoprefixer({ browsers: ['last 1 version'] }),
		precss,
		rucksack,
		pxtoem,
		customMedia,
		lost
		// customNested
	];
	return gulp.src(config.styles.toolkit)
		.pipe(plumber({ errorHandler: utils.errorHandler }))
		.pipe(postcss(processors))
		.pipe(gulp.dest(config.dest + '/assets/toolkit/styles'))
		.pipe(reload({stream:true}));
});