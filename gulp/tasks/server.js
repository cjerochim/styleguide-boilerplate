'use strict';

var gulp 						= require('gulp');
var browserSync 				= require('browser-sync');
var reload						= browserSync.reload;
var config 						= require('../config');


// server
gulp.task('serve', function () {
	browserSync({
		server: {
			baseDir: config.dest
		},
		notify: false,
		logPrefix: 'FABRICATOR'
	});
});