'use strict';


var gulp 					= require('gulp');
var imagemin 				= require('gulp-imagemin');
var config 					= require('../config');

gulp.task('images', function () {
	return gulp.src(config.images)
		.pipe(imagemin())
		.pipe(gulp.dest(config.dest + '/assets/toolkit/images'));
});