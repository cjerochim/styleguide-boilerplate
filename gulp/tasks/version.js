'use strict';

var gulp 				= require('gulp');
var bump 				= require('gulp-bump');
var changelog 			= require('gulp-conventional-changelog');
var config 				= require('../config');


gulp.task('version:changelog', function() {
	return gulp.src(config.version.file)
		.pipe(changelog({ 
			preset: 'angular',
			pkg: config.version.pkg 
		}))
		.pipe(gulp.dest(config.version.dest));
});


/**
 * Major push
 */
gulp.task('version:bump:minor', function() {
	return gulp.src(config.version.pkg)
		.pipe(bump({type:'minor'}))
		.pipe(gulp.dest('./'));
});

/**
 * Minor push
 */
gulp.task('version:bump:major', function() {
	return gulp.src(config.version.pkg)
		.pipe(bump({type:'major'}))
		.pipe(gulp.dest('./'));
});
